<?php namespace Elasticquent;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class ElasticquentPaginator extends Paginator
{
    /**
     * Create a new paginator instance.
     *
     * @param  mixed  $items
     * @param  int  $total
     * @param  int  $perPage
     * @param  int|null  $currentPage
     * @param  array  $options (path, query, fragment, pageName)
     */
    public function __construct($items, $total, $perPage, $currentPage = null, array $options = [])
    {
        foreach ($options as $key => $value) {
            $this->{$key} = $value;
        }
		if(is_array($total)) $total = $total['value'];
		$this->total = $total;
		if(is_array($this->total) && !empty($this->total['value'])) $this->total = $this->total['value'];
		$this->perPage = $perPage;
        $this->lastPage = (int) ceil($this->total / $perPage);
        $this->currentPage = $this->setCurrentPage($currentPage, $this->lastPage);
        $this->path = $this->path != '/' ? rtrim($this->path, '/') . '/' : $this->path;
        $this->items = $items instanceof Collection ? $items : Collection::make($items);
		$this->total = $total;
		parent::__construct($items, $total, $perPage);
    }

	/**
	 * Get the URL for a given page number.
	 *
	 * @param  int  $page
	 * @return string
	 */
	public function url($page)
	{
		if ($page <= 0) {
			$page = 1;
		}
		// If we have any extra query string key / value pairs that need to be added
		// onto the URL, we will put them in query string form and then attach it
		// to the URL. This allows for extra information like sortings storage.
		$parameters = [$this->pageName => $page];
		if($page == 1) $parameters = [];

		if (count($this->query) > 0) {
			$parameters = array_merge($this->query, $parameters);
		}

		return $this->path()
			.(Str::contains($this->path(), '?') ? '&' : (!empty($parameters) ? '?' : '') )
			.Arr::query($parameters)
			.$this->buildFragment();
	}

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'total'         => $this->total(),
            'per_page'      => $this->perPage(),
            'current_page'  => $this->currentPage(),
            'last_page'     => $this->lastPage(),
            'next_page_url' => $this->nextPageUrl(),
            'prev_page_url' => $this->previousPageUrl(),
            'from'          => $this->firstItem(),
            'to'            => $this->lastItem(),
            'data'          => $this->items->toArray(),
        ];
    }
}
